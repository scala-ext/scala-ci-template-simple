## Introduction

This is a simple pipeline for Scala with [SBT](https://www.scala-sbt.org/).

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)

If you are not familiar with scala you may want to view the tutorials for hello world [SCALA 3 — BOOK
HELLO, WORLD!](https://docs.scala-lang.org/scala3/book/taste-hello-world.html)

## What's contained in this project

- A simple 'hello world' example scala implementation.
- Additionally an example test using scalatest.


The `.gitlab-ci.yml` contains the configuration needed for GitLab to build your code.

In the case of this repo we use the eclipse-temurin 21.0.2 based docker image.
The scala version used is '3.4.0'.

```
sbtscala/scala-sbt:eclipse-temurin-jammy-21.0.2_13_1.9.9_3.4.0
```

To use another docker you can get more info from [DOCKER-SBT](https://github.com/sbt/docker-sbt) repository
Or directly by checking the available tags [SBTSCALA](https://hub.docker.com/r/sbtscala/scala-sbt/tags).

If the image changed the scala version might have to be updates as well.
In this case in `.gitlab-ci.yml` a variable is defined that can be changed appropriately.

```
variables:
  SCALA_VERSION: "3.4.0"
```

We're defining some common stages : `build`, `test` and `deploy``. 
If your project grows in complexity you can add more of these.

```
stages:
	- build
	- test
	- deploy
```

There are many, many powerful options for `.gitlab-ci.yml`. Read more information in the documentation documentation 
[here](https://docs.gitlab.com/ee/ci/yaml/).


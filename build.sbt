ThisBuild / scalaVersion := sys.env.getOrElse("SCALA_VERSION", "3.4.0")

lazy val hello = project
  .in(file("."))
  .settings(
    name := sys.env.getOrElse("SCALA_PROJECT_NAME", "scala-ci-template"),
    version := "1.0." + sys.env.getOrElse("BUILD_NUMBER", "0-SNAPSHOT"),
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.18" % "test"
  )

object HelloWorld {
    val HelloWorldReply = "Hello world!"

    def sayHello() : String = {
        return HelloWorldReply;
    }
}

@main
def printHelloWorld() : Unit = {
    println(HelloWorld.sayHello());
}
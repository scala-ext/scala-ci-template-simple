import org.scalatest._
import flatspec._
import matchers._

class HelloWorldSpec extends AnyFlatSpec with should.Matchers {
  "HelloWorld.sayHello" should "return 'Hello World!' " in {
    HelloWorld.sayHello() should be("Hello world!")
  }
}
